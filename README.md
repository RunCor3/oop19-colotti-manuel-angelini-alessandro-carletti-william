
# Simple ATC Simulator

Made using the following libraries:

- JavaFX
- JUnit

## Instructions

You can find an useful tutorial inside the application to understand how to play.

## Start the application

Download the SimpleATCSimulator jar.
Then use the command java -jar SimpleATCSimulator.jar in order to start the application. 

## Authors

- Angelini Alessandro
- Carletti William
- Colotti Manuel
- Lisotta Marco

## Programs used

- Eclipse IDE
- Scene Builder 2.0
